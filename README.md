# Oembed

Ce plugin pour SPIP permet d’insérer un contenu HTML à partir d’une simple URL, dans les textes des contenus éditoriaux (comme les articles). 

Par exemple insérer une vidéo dans le site à partir de l’URL d’une vidéo YouTube.

## Fonctionnement

Il y a 2 syntaxes valides

- l’URL, entourée de chevrons `<` et `>`
```
<https://...> 
```
- l’URL seule, entourée de sauts de ligne
```

https://...

```

